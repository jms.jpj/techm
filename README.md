# AngularJS Nokia Assignment

## Overview

This application is developed as part of Nokia assignment

The working demo can be found at http://www.jamespj.info:8080/app/

## Prerequisites

### Git

- A good place to learn about setting up git is [here][git-github].
- Git [home][git-home] (download, documentation).

### Node.js and Tools

- Get [Node.js][node-download].
- Install the tool dependencies (`npm install`).

For Ubuntu users use the following command
 - `sudo apt-get install nodejs-legacy npm`

### Running the app during development

- Run `npm start`
- navigate your browser to `http://localhost:8000/app/` to see the app running in your browser.

## Application Directory Layout

    app/                --> all of the files to be used in production
      css/              --> css files
        app.css         --> default stylesheet
      img/              --> image files
      index.html        --> app layout file (the main html template file of the app)
      js/               --> javascript files
        app.js          --> the main application module
        controllers.js  --> application controllers
        directives.js   --> application directives
        filters.js      --> custom angular filters
        services.js     --> custom angular services
      partials/         --> angular view partials (partial html templates) used by uiRouter
        header.html
        page.html
        content.html
        friends.html
        userinfo.html
        usernotes.html
      json/
        user.json
      bower_components  --> 3rd party js libraries, including angular and jquery
