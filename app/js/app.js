'use strict';

/* App Module */

var nokiaAssignment = angular.module('nokiaAssignment', [
  'ui.router', 'ui.bootstrap', 'ngMdIcons', 'nokiaAssignmentControllers', 
  'nokiaAssignmentServices', 'nokiaAssignmentDirectives', 'nokiaAssignmentFilters',
  'angular-owl-carousel-2'
]);

nokiaAssignment.config(['$locationProvider','$stateProvider', '$urlRouterProvider',
      function ($locationProvider, $stateProvider, $urlRouterProvider) {
//         $locationProvider.html5Mode(true);
        $stateProvider.state("home", {
          url: "/home",
          templateUrl: 'partials/page.html'
        }).state('home.info', {
          url: '/info',
          views: {
            "content@home": {
              templateUrl: 'partials/content.html'
            },
           "notes@home": {
               template: '<div data-user-friends></div>'
            },
            "albums@home": {
               template: '<div data-user-friends></div>'
             }
          }
        }).state('home.none', {
          url: '/none',
          views: {
            "none@home": {
              templateUrl: 'partials/none.html'
            }
          }
        })
        
        ;
        $urlRouterProvider.otherwise('/home/info');
  }]);
