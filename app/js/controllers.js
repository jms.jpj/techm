'use strict';

/* Controllers */

var nokiaAssignmentControllers = angular.module('nokiaAssignmentControllers', []);

nokiaAssignmentControllers.controller('HeaderCtrl', ['$window', function($window) {
    this.search = "";
    this.goBack = function(){
      $window.history.back();
    }
}]);

nokiaAssignmentControllers.controller('pageController', ['getUsers', 'userData',  function(getUsers, userData) {
    var pc = this;
    pc.userlist = {};
    pc.activeUser = [];
    pc.grid = true;  
    
    getUsers.then(function(res){
      pc.userlist = res.data.users;
      pc.activeUser = pc.userlist[0];
      userData.save(res);
    });
    pc.randomNum = function(){
      return Math.floor(Math.random() * (2222 - 1111)) + 1111;
    }
    pc.changeView = function(view){
      if(view == "grid"){
        pc.grid = true;
      }else if(view == "list"){
        pc.grid = false;
      }
    }
  }]);

nokiaAssignmentControllers.controller('notesCtrl', [function() {
  var nt = this;
  nt.topo =   [
    {name: "VoIP SIP Service", color: "#ff98a9"},
    {name: "ONT", color: "#ff98a9"},
    {name: "IPHOST", color: "#fecc87"},
    {name: "VEIP", color: "#fecc87"}
  ];
  nt.slides =   [
    {id: 0, color: "#a5ef9a", icon: "check_circle", fill: "#4d6d46"},
    {id: 1, color: "#a5ef9a", icon: "credit_card", fill: "#4d6d46"},
    {id: 2, color: "#fecc87", icon: "face", fill: "#4d6d46"},
    {id: 3, color: "#a5ef9a", icon: "language", fill: "#4d6d46"},
    {id: 4, color: "#fecc87", icon: "fingerprint", fill: "#4d6d46"},
  ];  
  nt.prop = {
    loop:true,
    margin:0,
    responsiveClass:true,
    items:4,
    responsive : {
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    },
    nav:true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
  };
  nt.nttable =   {
    th: [
      'ID', 'Slot:IACM: R1.S1.LT7 (FWLTA-A)', 
      'Channel Pair: IACM:R1.S1.LT7.CP1 (DefaultChannel1, CG10.SCG1 (ERI200AIR60))',
      'ChannelGroup: IACM:CG10 (ERI200AIR60)',
      'Sub Channel Group: IACM:CG10.SCG1 (ERI200AIR60)'
    ],
    tr: [
      [
        {color: "#757575", back: "", label: "Object Alarms", outer: ""},
        {color: "#000", back: "#a3e1b8", label: "0", outer: ""},
        {color: "#000", back: "#a3e1b8", label: "0", outer: ""},
        {color: "#000", back: "#a3e1b8", label: "0", outer: ""},
        {color: "#000", back: "#a3e1b8", label: "0", outer: ""}
      ],
      [
        {color: "#757575", back: "", label: "Aggregate Alarms", outer: ""},
        {color: "#fff", back: "#da2b26", label: "c", outer: "1"},
        {color: "#fff", back: "#da2b26", label: "c", outer: "1"},
        {color: "#fff", back: "#da2b26", label: "c", outer: "1"},
        {color: "#fff", back: "#da2b26", label: "c", outer: "1"}
      ]
    ],
    tr1: [
      [
        {color: "#757575",  label: "Operational Status", icon: ""},
        {color: "#d82163", label: "", icon: "arrow_downward", fill: "#d82163"},
        {color: "#02a89a", label: "", icon: "arrow_upwards", fill: "#02a89a"},
        {color: "#02a89a", label: "", icon: "arrow_upwards", fill: "#02a89a"},
        {color: "#02a89a", label: "", icon: "arrow_upwards", fill: "#02a89a"}
      ],
      [
        {color: "#757575", label: "Admin Status", icon: ""},
        {color: "#fff", label: "", icon: "lock", fill: "#767676"},
        {color: "#fff", label: "", icon: "lock_open", fill: "#747474"},
        {color: "#fff", label: "", icon: "lock_open", fill: "#747474"},
        {color: "#fff", label: "", icon: "lock_open", fill: "#747474"}
      ]
    ]
};
  nt.toggleTable = function(event, _this){
    var target = angular.element(angular.element(event.target).data('target')), clicked = angular.element(event.target);
    if(target.hasClass('collapsed')){
      target.removeClass('collapsed').addClass('collapse');
      clicked.html('<i class="fa-angle-down fa"></i> Expand');
    }else{
      target.removeClass('collapse').addClass('collapsed');
      clicked.html('<i class="fa-angle-up fa"></i> Collapse');
    }
  }
}]);