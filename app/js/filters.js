'use strict';

/* Filters */
var nokiaAssignmentFilters = angular.module('nokiaAssignmentFilters', []);
        
nokiaAssignmentFilters.filter('contains', function() {
  return function (array, needle) {
    return array.indexOf(needle) >= 0;
  };
});