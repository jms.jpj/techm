'use strict';

/* Directives */

var nokiaAssignmentDirectives = angular.module('nokiaAssignmentDirectives', []);

nokiaAssignmentDirectives.directive('userFriends', ['userData', function(userData){
  return {
    restrict: 'A',
    templateUrl: 'partials/friends.html',
    link: function(scope, ele, attr, ctrl){
      ele.on('click', '.friend', function(){
        angular.element(this).toggleClass('selected unselected');
      });
    }
  }
}]).directive('userInfo', [function(){
  return {
    restrict: 'A',
    templateUrl: 'partials/userinfo.html',
  }
}]).directive('userNotes', [function(){
  return {
    restrict: 'A',
    templateUrl: 'partials/usernotes.html',
  }
}]);