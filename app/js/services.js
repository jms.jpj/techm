'use strict';

/* Services */

var nokiaAssignmentServices = angular.module('nokiaAssignmentServices', []);

nokiaAssignmentServices.factory('getUsers', ['$http',  function($http){
    var baseUrl = window.location.protocol + window.location.host + window.location.pathname;
    
    return $http({
      'url': 'json/user.json',
      'method': 'GET'
    });
  }]).
  service('userData', [function(){
    this.users = {};
    this.save = function(users){
        this.users = users;
    };
    this.read = function(){
      return this.users;
    }
  }]);
